package service

import (
	"context"
	"database/sql"
	"iceller-store/entity"
	"iceller-store/helper"
	"iceller-store/repository"
	"iceller-store/web"
)

type GadgetService interface {
	FindAll(ctx context.Context) ([]entity.Gadget, error)
	FIndByName(ctx context.Context, request web.GadgetNameRequest) ([]entity.Gadget, error)
}

type gadgetService struct {
	gadgetRepository repository.GadgetRepository
	DB               *sql.DB
}

func NewGadgetService(gadgetRepository repository.GadgetRepository, db *sql.DB) GadgetService {
	return &gadgetService{gadgetRepository: gadgetRepository, DB: db}
}

func (service *gadgetService) FindAll(ctx context.Context) ([]entity.Gadget, error) {
	gadgets, err := service.gadgetRepository.FindAll(ctx)
	helper.PanicIfError(err)

	return gadgets, nil
}

func (service *gadgetService) FIndByName(ctx context.Context, request web.GadgetNameRequest) ([]entity.Gadget, error) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	gadgets, err := service.gadgetRepository.FindByName(ctx, request.Name, tx)
	helper.PanicIfError(err)

	return gadgets, nil

}
