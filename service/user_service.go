package service

import (
	"context"
	"errors"
	"github.com/go-playground/validator/v10"
	"golang.org/x/crypto/bcrypt"
	"iceller-store/entity"
	"iceller-store/helper"
	"iceller-store/repository"
	"iceller-store/web"
	"time"
)

type UserService interface {
	Create(ctx context.Context, request web.RegisterUser) (entity.User, error)
	LoginUser(ctx context.Context, request web.UserLoginRequest) (entity.User, error)
	GetById(ctx context.Context, userId int) (entity.User, error)
}

type userService struct {
	userRepository repository.UserRepository
	validate       *validator.Validate
}

func NewUserService(userRepository repository.UserRepository, validate *validator.Validate) UserService {
	return &userService{userRepository: userRepository, validate: validate}
}

func (service *userService) Create(ctx context.Context, request web.RegisterUser) (entity.User, error) {
	var user entity.User
	err := service.validate.Struct(request)
	if err != nil {
		return user, err
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.MinCost)
	helper.PanicIfError(err)

	user = entity.User{
		Email:     request.Email,
		Password:  string(passwordHash),
		FullName:  request.FullName,
		Role:      "user",
		Status:    true,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	newUser, err := service.userRepository.Create(ctx, user)
	helper.PanicIfError(err)

	return newUser, nil
}

func (service *userService) LoginUser(ctx context.Context, request web.UserLoginRequest) (entity.User, error) {
	var user entity.User
	err := service.validate.Struct(request)
	if err != nil {
		return user, err
	}

	user, err = service.userRepository.FindByEmail(ctx, request.Email)
	helper.PanicIfError(err)

	if user.Id == 0 {
		return user, errors.New("no user found for that email")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))
	if err != nil {
		return user, errors.New("incorrect password")
	}

	return user, nil
}

func (service *userService) GetById(ctx context.Context, userId int) (entity.User, error) {
	userData, err := service.userRepository.FindById(ctx, userId)
	helper.PanicIfError(err)

	if userData.Id == 0 {
		return userData, errors.New("no user found for that email")
	}

	return userData, nil
}
