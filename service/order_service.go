package service

import (
	"context"
	"database/sql"
	"errors"
	"iceller-store/entity"
	"iceller-store/helper"
	"iceller-store/repository"
)

type OrderService interface {
	Create(ctx context.Context, id int) ([]entity.Order, error)
	GetByUserId(ctx context.Context, userId int) ([]entity.Order, error)
}

type orderService struct {
	orderRepository  repository.OrderRepository
	cartRepository   repository.CartRepository
	gadgetRepository repository.GadgetRepository
	DB               *sql.DB
}

func NewOrderService(orderRepository repository.OrderRepository, cartRepository repository.CartRepository, gadgetRepository repository.GadgetRepository, db *sql.DB) OrderService {
	return &orderService{orderRepository: orderRepository, cartRepository: cartRepository, gadgetRepository: gadgetRepository, DB: db}
}

func (service *orderService) Create(ctx context.Context, id int) ([]entity.Order, error) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	var orders []entity.Order
	carts, err := service.cartRepository.GetByUserId(ctx, id, tx)
	helper.PanicIfError(err)
	if length := len(carts); length == 0 {
		return orders, errors.New("Tidak dapat chekcout, cart kosong")
	}

	orders, err = service.orderRepository.Create(ctx, carts, tx)
	helper.PanicIfError(err)

	service.gadgetRepository.UpdateStock(ctx, orders, tx)

	service.cartRepository.DeleteByUserId(ctx, id, tx)

	return orders, nil
}

func (service *orderService) GetByUserId(ctx context.Context, userId int) ([]entity.Order, error) {
	orders, err := service.orderRepository.GetByUserId(ctx, userId)
	helper.PanicIfError(err)

	return orders, nil
}
