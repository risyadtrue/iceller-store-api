package service

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"iceller-store/helper"
)

type AuthService interface {
	GenerateTokens(userId int) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}

type jwtService struct {
}

func NewAuthService() AuthService {
	return &jwtService{}
}

var SECRET_KEY = []byte("LogkarManiaMantap")

func (service *jwtService) GenerateTokens(userId int) (string, error) {
	claim := jwt.MapClaims{}
	// add to token payload
	claim["userId"] = userId

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	signedToken, err := token.SignedString(SECRET_KEY)
	helper.PanicIfError(err)

	return signedToken, nil
}

func (service *jwtService) ValidateToken(token string) (*jwt.Token, error) {
	tkn, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, errors.New("Invalid Token")
		}

		return []byte(SECRET_KEY), nil
	})

	if err != nil {
		return tkn, nil
	}

	return tkn, nil
}
