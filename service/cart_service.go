package service

import (
	"context"
	"database/sql"
	"iceller-store/entity"
	"iceller-store/helper"
	"iceller-store/repository"
	"iceller-store/web"
	"time"
)

type CartService interface {
	Create(ctx context.Context, request web.CartRequest) ([]entity.Cart, error)
	Delete(ctx context.Context, id int)
	BulkCreate(ctx context.Context, request web.CartRequestBulk) ([]entity.Cart, error)
}

type cartService struct {
	cartRepository   repository.CartRepository
	gadgetRepository repository.GadgetRepository
	DB               *sql.DB
}

func NewCartService(cartRepository repository.CartRepository, db *sql.DB, gadgetRepository repository.GadgetRepository) CartService {
	return &cartService{cartRepository: cartRepository, DB: db, gadgetRepository: gadgetRepository}
}

func (service *cartService) Create(ctx context.Context, request web.CartRequest) ([]entity.Cart, error) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	var carts []entity.Cart

	cart := entity.Cart{
		UserId:    request.UserId,
		GadgetId:  request.GadgetId,
		Quantity:  request.Quantity,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	carts = append(carts, cart)

	newCarts, err := service.cartRepository.Create(ctx, carts, tx)
	helper.PanicIfError(err)

	return newCarts, nil
}

func (service *cartService) BulkCreate(ctx context.Context, request web.CartRequestBulk) ([]entity.Cart, error) {
	// map gadgetId:cartQuantity
	cartsMap := make(map[int]*web.BulkCartRequestValue)

	// manipulation quantity for same gadget id
	for _, req := range request.BulkCartRequestValue {
		var temp int
		if x, found := cartsMap[req.GadgetId]; found {
			temp = x.Quantity
		}

		bulkCartValues := web.BulkCartRequestValue{
			GadgetId: req.GadgetId,
			Quantity: req.Quantity + temp,
		}

		cartsMap[req.GadgetId] = &bulkCartValues
	}

	// mapping map to entity cart
	var carts []entity.Cart
	var gadgetsId []int
	for key, value := range cartsMap {
		cart := entity.Cart{
			UserId:   request.UserId,
			GadgetId: key,
			Quantity: value.Quantity,
		}

		carts = append(carts, cart)
		gadgetsId = append(gadgetsId, key)
	}

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	_, err = service.gadgetRepository.FindById(ctx, gadgetsId, tx)
	if err != nil {
		return carts, err
	}

	newCarts, err := service.cartRepository.Create(ctx, carts, tx)
	if err != nil {
		return newCarts, err
	}

	return newCarts, nil
}

func (service *cartService) Delete(ctx context.Context, id int) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)
	cartData, err := service.cartRepository.GetById(ctx, id, tx)
	helper.PanicIfError(err)

	service.cartRepository.Delete(ctx, cartData, tx)
}
