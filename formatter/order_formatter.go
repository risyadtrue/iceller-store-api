package formatter

import (
	"iceller-store/entity"
	"time"
)

type OrderFormatter struct {
	Id        int       `json:"id"`
	UserId    int       `json:"user_id"`
	GadgetId  int       `json:"gadget_id"`
	Total     int       `json:"total"`
	Quantity  int       `json:"quantity"`
	Status    string    `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func OrderFormatterResponse(order entity.Order) OrderFormatter {
	return OrderFormatter{
		Id:        order.Id,
		UserId:    order.UserId,
		GadgetId:  order.GadgetId,
		Total:     order.Total,
		Quantity:  order.Quantity,
		Status:    order.Status,
		CreatedAt: order.CreatedAt,
		UpdatedAt: order.UpdatedAt,
	}
}

func OrderFormatterResponses(orders []entity.Order) []OrderFormatter {
	var orderResponses []OrderFormatter
	for _, order := range orders {
		orderResponse := OrderFormatterResponse(order)
		orderResponses = append(orderResponses, orderResponse)
	}

	return orderResponses
}

type OrderHistory struct {
	Name     string `json:"name"`
	Quantity int    `json:"quantity"`
	Total    int    `json:"total"`
	Status   string `json:"status"`
}

func OrderHistoryResponse(order entity.Order) OrderHistory {
	return OrderHistory{
		Name:     order.Gadget.Name,
		Quantity: order.Quantity,
		Total:    order.Total,
		Status:   order.Status,
	}
}

func OrderHistoryResponses(orders []entity.Order) []OrderHistory {
	var orderHistoryResponses []OrderHistory

	for _, order := range orders {
		orderHistory := OrderHistoryResponse(order)
		orderHistoryResponses = append(orderHistoryResponses, orderHistory)
	}

	return orderHistoryResponses
}
