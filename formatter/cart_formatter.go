package formatter

import "iceller-store/entity"

type CartFormatter struct {
	Id       int `json:"id"`
	UserId   int `json:"user_id"`
	GadgetId int `json:"gadget_id"`
	Quantity int `json:"quantity"`
}

func CartFormatterResponse(cart entity.Cart) CartFormatter {
	return CartFormatter{
		Id:       cart.Id,
		UserId:   cart.UserId,
		GadgetId: cart.GadgetId,
		Quantity: cart.Quantity,
	}
}

func CartFormatterResponses(carts []entity.Cart) []CartFormatter {
	var cartFormatterResponses []CartFormatter
	for _, cart := range carts {
		cartFormatter := CartFormatterResponse(cart)
		cartFormatterResponses = append(cartFormatterResponses, cartFormatter)
	}

	return cartFormatterResponses
}
