package formatter

import (
	"iceller-store/entity"
	"time"
)

type GadgetFormatter struct {
	Id          int       `json:"id"`
	Name        string    `json:"name"`
	Price       int       `json:"price"`
	Stock       int       `json:"stock"`
	Description string    `json:"description"`
	Storage     string    `json:"storage"`
	RAM         string    `json:"ram"`
	Color       string    `json:"color"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func FormatGadgetResponse(gadget entity.Gadget) GadgetFormatter {
	formatter := GadgetFormatter{
		Id:          gadget.Id,
		Name:        gadget.Name,
		Price:       gadget.Price,
		Stock:       gadget.Stock,
		Description: gadget.Description,
		Storage:     gadget.Storage,
		RAM:         gadget.RAM,
		Color:       gadget.Color,
		CreatedAt:   gadget.CreatedAt,
		UpdatedAt:   gadget.UpdatedAt,
	}

	return formatter
}

func FormatGadgetResponses(gadgets []entity.Gadget) []GadgetFormatter {
	var gadgetsFormatter []GadgetFormatter

	for _, gadget := range gadgets {
		formatter := FormatGadgetResponse(gadget)
		gadgetsFormatter = append(gadgetsFormatter, formatter)
	}

	return gadgetsFormatter
}
