package formatter

import (
	"iceller-store/entity"
	"time"
)

type UserFormatter struct {
	Id        int       `json:"id"`
	Email     string    `json:"email"`
	FullName  string    `json:"full_name"`
	Role      string    `json:"role"`
	Status    bool      `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Token     string    `json:"token"`
}

type LoginUserFomatter struct {
	Email    string `json:"email"`
	FullName string `json:"fullname"`
	Token    string `json:"token"`
}

func FormatUserResponse(user entity.User, token string) UserFormatter {
	formatter := UserFormatter{
		Id:        user.Id,
		Email:     user.Email,
		FullName:  user.FullName,
		Role:      user.Role,
		Status:    user.Status,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
		Token:     token,
	}

	return formatter
}

func FormatLoggedInUserResponse(user entity.User, token string) LoginUserFomatter {
	formatter := LoginUserFomatter{
		Email:    user.Email,
		FullName: user.FullName,
		Token:    token,
	}

	return formatter
}
