package handler

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"iceller-store/entity"
	formatter2 "iceller-store/formatter"
	"iceller-store/helper"
	"iceller-store/service"
	"iceller-store/web"
	"net/http"
	"strconv"
)

type CartHandler interface {
	AddToCart(writer http.ResponseWriter, request *http.Request)
	DeleteCart(writer http.ResponseWriter, request *http.Request)
	BulkAddToCart(writer http.ResponseWriter, request *http.Request)
}

type cartHandler struct {
	cartService service.CartService
}

func NewCartHandler(cartService service.CartService) CartHandler {
	return &cartHandler{cartService: cartService}
}

func (handler *cartHandler) AddToCart(writer http.ResponseWriter, request *http.Request) {
	decoder := json.NewDecoder(request.Body)
	cartRequest := web.CartRequest{}
	err := decoder.Decode(&cartRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	userData := request.Context().Value("user").(entity.User)
	cartRequest.UserId = userData.Id

	gadgetId := chi.URLParam(request, "gadgetId")
	cartRequest.GadgetId, err = strconv.Atoi(gadgetId)

	_, err = handler.cartService.Create(request.Context(), cartRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "Gadget berhasil ditambahkan kedalam Cart", nil)

	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *cartHandler) DeleteCart(writer http.ResponseWriter, request *http.Request) {
	param := chi.URLParam(request, "cartId")
	cartId, err := strconv.Atoi(param)
	helper.PanicIfError(err)

	handler.cartService.Delete(request.Context(), cartId)

	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "Cart berhasil dihapus", nil)

	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *cartHandler) BulkAddToCart(writer http.ResponseWriter, request *http.Request) {
	//var cartsRequest []web.CartRequest
	var cartRequest web.CartRequestBulk

	userData := request.Context().Value("user").(entity.User)
	cartRequest.UserId = userData.Id

	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&cartRequest.BulkCartRequestValue)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	carts, err := handler.cartService.BulkCreate(request.Context(), cartRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatter := formatter2.CartFormatterResponses(carts)
	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "Cart berhasil ditambahkan", formatter)

	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)

}
