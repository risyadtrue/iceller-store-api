package handler

import (
	"encoding/json"
	"errors"
	"iceller-store/formatter"
	"iceller-store/helper"
	"iceller-store/service"
	"iceller-store/web"
	"net/http"
)

type CategoryHandler interface {
	FindAll(writer http.ResponseWriter, request *http.Request)
	FindByName(writer http.ResponseWriter, request *http.Request)
}

type categoryHandler struct {
	categoryService service.GadgetService
}

func NewGadgetController(categoryService service.GadgetService) CategoryHandler {
	return &categoryHandler{categoryService}
}

func (handler *categoryHandler) FindAll(writer http.ResponseWriter, request *http.Request) {
	gadgetResponses, err := handler.categoryService.FindAll(request.Context())
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}
	formatData := formatter.FormatGadgetResponses(gadgetResponses)
	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "success get all gadget", formatData)

	writer.Header().Add("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *categoryHandler) FindByName(writer http.ResponseWriter, request *http.Request) {
	decoder := json.NewDecoder(request.Body)
	gadgetNameRequest := web.GadgetNameRequest{}
	err := decoder.Decode(&gadgetNameRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	gadgetResponses, err := handler.categoryService.FIndByName(request.Context(), gadgetNameRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatData := formatter.FormatGadgetResponses(gadgetResponses)

	message := "Success get gadget data for " + gadgetNameRequest.Name
	webResponse := helper.WebResponseTemplate("success", 200, message, formatData)

	if formatData == nil {
		helper.ErrorResponseHandler(writer, http.StatusNotFound, errors.New(gadgetNameRequest.Name+" gadget not found"))
		return
	}

	writer.Header().Add("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}
