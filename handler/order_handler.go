package handler

import (
	"encoding/json"
	"iceller-store/entity"
	"iceller-store/formatter"
	"iceller-store/helper"
	"iceller-store/service"
	"net/http"
)

type OrderHandler interface {
	CheckOut(writer http.ResponseWriter, request *http.Request)
	GetOrderHistory(writer http.ResponseWriter, request *http.Request)
}

type orderHandler struct {
	orderService service.OrderService
}

func NewOrderHandler(orderService service.OrderService) OrderHandler {
	return &orderHandler{orderService: orderService}
}

func (handler *orderHandler) CheckOut(writer http.ResponseWriter, request *http.Request) {
	userData := request.Context().Value("user").(entity.User)

	orders, err := handler.orderService.Create(request.Context(), userData.Id)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatData := formatter.OrderFormatterResponses(orders)
	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "Cart berhasil di check out", formatData)

	writer.Header().Add("Content-type", "application/json")
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *orderHandler) GetOrderHistory(writer http.ResponseWriter, request *http.Request) {
	userData := request.Context().Value("user").(entity.User)

	orders, err := handler.orderService.GetByUserId(request.Context(), userData.Id)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatData := formatter.OrderHistoryResponses(orders)
	webResponse := helper.WebResponseTemplate("success", http.StatusOK, "Riwayat pembelian user", formatData)

	writer.Header().Add("Content-type", "application/json")
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)

}
