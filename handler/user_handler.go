package handler

import (
	"encoding/json"
	"iceller-store/entity"
	"iceller-store/formatter"
	"iceller-store/helper"
	"iceller-store/service"
	"iceller-store/web"
	"net/http"
)

type UserHandler interface {
	Create(writer http.ResponseWriter, request *http.Request)
	LoginUser(writer http.ResponseWriter, request *http.Request)
	GetLoggedInUser(writer http.ResponseWriter, request *http.Request)
}

type userHandler struct {
	userService service.UserService
	authService service.AuthService
}

func NewUserHandler(userService service.UserService, authService service.AuthService) UserHandler {
	return &userHandler{userService: userService, authService: authService}
}

func (handler *userHandler) Create(writer http.ResponseWriter, request *http.Request) {
	decoder := json.NewDecoder(request.Body)
	registerUserRequest := web.RegisterUser{}
	err := decoder.Decode(&registerUserRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	newUser, err := handler.userService.Create(request.Context(), registerUserRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	token, err := handler.authService.GenerateTokens(newUser.Id)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatData := formatter.FormatUserResponse(newUser, token)
	webResponse := helper.WebResponseTemplate("success", 200, "register success", formatData)

	writer.Header().Add("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *userHandler) LoginUser(writer http.ResponseWriter, request *http.Request) {
	// mapping from request body (stream) to request struct
	decoder := json.NewDecoder(request.Body)
	userLoginRequest := web.UserLoginRequest{}
	err := decoder.Decode(&userLoginRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	loggedInUser, err := handler.userService.LoginUser(request.Context(), userLoginRequest)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	token, err := handler.authService.GenerateTokens(loggedInUser.Id)
	if err != nil {
		helper.ErrorResponseHandler(writer, http.StatusBadRequest, err)
		return
	}

	formatData := formatter.FormatLoggedInUserResponse(loggedInUser, token)
	webResponse := helper.WebResponseTemplate("success", 200, "login success", formatData)

	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	helper.PanicIfError(err)
}

func (handler *userHandler) GetLoggedInUser(writer http.ResponseWriter, request *http.Request) {
	userData := request.Context().Value("user").(entity.User)

	formatData := formatter.FormatLoggedInUserResponse(userData, " ")
	webResponse := helper.WebResponseTemplate("success", 200, "login success", formatData)

	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(writer)
	err := encoder.Encode(webResponse)
	helper.PanicIfError(err)
}
