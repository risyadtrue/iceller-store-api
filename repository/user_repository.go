package repository

import (
	"context"
	"database/sql"
	"iceller-store/entity"
	"iceller-store/helper"
)

type UserRepository interface {
	Create(ctx context.Context, user entity.User) (entity.User, error)
	FindByEmail(ctx context.Context, email string) (entity.User, error)
	FindById(ctx context.Context, userId int) (entity.User, error)
}

type userRepository struct {
	DB *sql.DB
}

func NewUserRepository(db *sql.DB) UserRepository {
	return &userRepository{db}
}

func (repository *userRepository) Create(ctx context.Context, user entity.User) (entity.User, error) {
	lastInsertId := 0
	query := "INSERT INTO users (email, password, fullname, role, status, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id"
	// biasanya pake ExecContex
	err := repository.DB.QueryRow(query, user.Email, user.Password, user.FullName, user.Role, user.Status, user.CreatedAt, user.UpdatedAt).Scan(&lastInsertId)
	helper.PanicIfError(err)

	user.Id = lastInsertId

	return user, nil
}

func (repository *userRepository) FindByEmail(ctx context.Context, email string) (entity.User, error) {
	query := "SELECT id, email, password, fullname FROM users WHERE email = $1"
	row, err := repository.DB.QueryContext(ctx, query, email)
	helper.PanicIfError(err)

	user := entity.User{}
	if row.Next() {
		err := row.Scan(&user.Id, &user.Email, &user.Password, &user.FullName)
		helper.PanicIfError(err)
	}

	return user, nil
}

func (repository *userRepository) FindById(ctx context.Context, userId int) (entity.User, error) {
	query := "SELECT id, email, password, fullname FROM users WHERE id = $1"
	row, err := repository.DB.QueryContext(ctx, query, userId)
	helper.PanicIfError(err)

	user := entity.User{}
	if row.Next() {
		err := row.Scan(&user.Id, &user.Email, &user.Password, &user.FullName)
		helper.PanicIfError(err)
	}

	return user, nil
}
