package repository

import (
	"context"
	"database/sql"
	"errors"
	"iceller-store/entity"
	"iceller-store/helper"
	"strings"
)

type GadgetRepository interface {
	FindAll(ctx context.Context) ([]entity.Gadget, error)
	FindByName(ctx context.Context, name string, tx *sql.Tx) ([]entity.Gadget, error)
	UpdateStock(ctx context.Context, orders []entity.Order, tx *sql.Tx)
	FindById(ctx context.Context, ids []int, tx *sql.Tx) ([]entity.Gadget, error)
}

type gadgetRepository struct {
	DB *sql.DB
}

func NewGadgetRepository(db *sql.DB) GadgetRepository {
	return &gadgetRepository{DB: db}
}

func (repository *gadgetRepository) FindAll(ctx context.Context) ([]entity.Gadget, error) {
	query := "SELECT id, name, price, stock, description, storage, ram, color, created_at, updated_at FROM gadgets"
	rows, err := repository.DB.QueryContext(ctx, query)
	helper.PanicIfError(err)

	var gadgets []entity.Gadget
	for rows.Next() {
		gadget := entity.Gadget{}
		err := rows.Scan(&gadget.Id, &gadget.Name, &gadget.Price, &gadget.Stock, &gadget.Description, &gadget.Storage, &gadget.RAM, &gadget.Color, &gadget.CreatedAt, &gadget.UpdatedAt)
		helper.PanicIfError(err)

		gadgets = append(gadgets, gadget)
	}

	return gadgets, nil
}

func (repository *gadgetRepository) FindById(ctx context.Context, ids []int, tx *sql.Tx) ([]entity.Gadget, error) {
	query := "SELECT id, name, price, stock, description, storage, ram, color, created_at, updated_at FROM gadgets WHERE id = $1"
	//statement, err := tx.PrepareContext(ctx, query)
	//helper.PanicIfError(err)
	//defer statement.Close()

	var gadgets []entity.Gadget
	for _, id := range ids {
		gadget := entity.Gadget{}
		row, err := repository.DB.QueryContext(ctx, query, id)
		helper.PanicIfError(err)

		if row.Next() {
			err = row.Scan(&gadget.Id, &gadget.Name, &gadget.Price, &gadget.Stock, &gadget.Description, &gadget.Storage, &gadget.RAM, &gadget.Color, &gadget.CreatedAt, &gadget.UpdatedAt)
			helper.PanicIfError(err)
		} else {
			return gadgets, errors.New("gadget not found")
		}

		gadgets = append(gadgets, gadget)
	}

	return gadgets, nil
}

func (repository *gadgetRepository) FindByName(ctx context.Context, name string, tx *sql.Tx) ([]entity.Gadget, error) {
	query := "SELECT id, name, price, stock, description, storage, ram, color, created_at, updated_at FROM gadgets WHERE LOWER (name) = $1"
	rows, err := tx.QueryContext(ctx, query, strings.ToLower(name))
	helper.PanicIfError(err)

	var gadgets []entity.Gadget
	for rows.Next() {
		gadget := entity.Gadget{}
		err := rows.Scan(&gadget.Id, &gadget.Name, &gadget.Price, &gadget.Stock, &gadget.Description, &gadget.Storage, &gadget.RAM, &gadget.Color, &gadget.CreatedAt, &gadget.UpdatedAt)
		helper.PanicIfError(err)

		gadgets = append(gadgets, gadget)
	}

	return gadgets, nil
}

func (repository *gadgetRepository) UpdateStock(ctx context.Context, orders []entity.Order, tx *sql.Tx) {
	query := "UPDATE gadgets SET stock = stock - $1 WHERE id = $2"
	statement, err := tx.PrepareContext(ctx, query)
	helper.PanicIfError(err)
	defer statement.Close()

	for _, order := range orders {
		_, err := statement.ExecContext(ctx, order.Quantity, order.GadgetId)
		helper.PanicIfError(err)
	}
}
