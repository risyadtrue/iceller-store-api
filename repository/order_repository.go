package repository

import (
	"context"
	"database/sql"
	"iceller-store/entity"
	"iceller-store/helper"
)

type OrderRepository interface {
	Create(ctx context.Context, carts []entity.Cart, tx *sql.Tx) ([]entity.Order, error)
	GetByUserId(ctx context.Context, userId int) ([]entity.Order, error)
}

type orderRepository struct {
	DB *sql.DB
}

func NewOrderRepository(db *sql.DB) OrderRepository {
	return &orderRepository{DB: db}
}

func (repository *orderRepository) Create(ctx context.Context, carts []entity.Cart, tx *sql.Tx) ([]entity.Order, error) {
	query := "INSERT INTO orders (user_id, gadget_id, total, quantity, status, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)"
	statement, err := tx.PrepareContext(ctx, query)
	helper.PanicIfError(err)
	defer statement.Close()

	var orders []entity.Order
	status := "success"
	for _, cart := range carts {
		_, err := statement.ExecContext(ctx, cart.UserId, cart.GadgetId, cart.Total, cart.Quantity, status, cart.CreatedAt, cart.UpdatedAt)
		helper.PanicIfError(err)

		order := entity.Order{
			UserId:    cart.UserId,
			GadgetId:  cart.GadgetId,
			Total:     cart.Total,
			Status:    status,
			Quantity:  cart.Quantity,
			CreatedAt: cart.CreatedAt,
			UpdatedAt: cart.UpdatedAt,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (repository *orderRepository) GetByUserId(ctx context.Context, userId int) ([]entity.Order, error) {
	query := "SELECT gadgets.name, orders.quantity, orders.total, orders.status FROM orders INNER JOIN gadgets ON gadgets.id = orders.gadget_id WHERE user_id = $1;"
	rows, err := repository.DB.QueryContext(ctx, query, userId)
	helper.PanicIfError(err)

	var orders []entity.Order
	for rows.Next() {
		order := entity.Order{}
		err := rows.Scan(&order.Gadget.Name, &order.Quantity, &order.Total, &order.Status)
		helper.PanicIfError(err)
		orders = append(orders, order)
	}

	return orders, nil
}
