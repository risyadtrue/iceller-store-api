package repository

import (
	"context"
	"database/sql"
	"iceller-store/entity"
	"iceller-store/helper"
)

type CartRepository interface {
	GetById(ctx context.Context, id int, tx *sql.Tx) (entity.Cart, error)
	GetByUserId(ctx context.Context, id int, tx *sql.Tx) ([]entity.Cart, error)
	Create(ctx context.Context, cart []entity.Cart, tx *sql.Tx) ([]entity.Cart, error)
	Delete(ctx context.Context, cart entity.Cart, tx *sql.Tx)
	DeleteByUserId(ctx context.Context, userId int, tx *sql.Tx)
}

type cartRepository struct {
	DB *sql.DB
}

func NewCartRepository(db *sql.DB) CartRepository {
	return &cartRepository{DB: db}
}

func (repository *cartRepository) GetById(ctx context.Context, id int, tx *sql.Tx) (entity.Cart, error) {
	query := "SELECT id, user_id, gadget_id, quantity FROM carts WHERE id = $1"
	row, err := tx.QueryContext(ctx, query, id)
	helper.PanicIfError(err)
	defer row.Close()

	cart := entity.Cart{}
	if row.Next() {
		err := row.Scan(&cart.Id, &cart.UserId, &cart.GadgetId, &cart.Quantity)
		helper.PanicIfError(err)
	}

	return cart, nil
}

func (repository *cartRepository) GetByUserId(ctx context.Context, id int, tx *sql.Tx) ([]entity.Cart, error) {
	//query := "SELECT carts.id, users.fullname, gadgets.name, carts.quantity FROM users INNER JOIN carts ON users.id = carts.user_id INNER JOIN gadgets ON gadgets.id = carts.gadget_id WHERE users.id = $1"
	query := "SELECT carts.id, carts.user_id, carts.gadget_id, carts.quantity, (carts.quantity * gadgets.price), carts.created_at, carts.updated_at FROM carts INNER JOIN gadgets ON carts.gadget_id = gadgets.id WHERE user_id = $1"
	rows, err := tx.QueryContext(ctx, query, id)
	helper.PanicIfError(err)
	defer rows.Close()

	var carts []entity.Cart
	for rows.Next() {
		cart := entity.Cart{}
		//err := rows.Scan(&cart.Id, &cart.User.FullName, &cart.Gadget.Name, &cart.Quantity)
		err := rows.Scan(&cart.Id, &cart.UserId, &cart.GadgetId, &cart.Quantity, &cart.Total, &cart.CreatedAt, &cart.UpdatedAt)
		helper.PanicIfError(err)
		carts = append(carts, cart)
	}

	return carts, nil
}

func (repository *cartRepository) Create(ctx context.Context, carts []entity.Cart, tx *sql.Tx) ([]entity.Cart, error) {
	query := "INSERT INTO carts (user_id, gadget_id, quantity, created_at, updated_at) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	statement, err := tx.PrepareContext(ctx, query)
	helper.PanicIfError(err)
	defer statement.Close()
	// _, err := repository.DB.ExecContext(ctx, query, cart.UserId, cart.GadgetId, cart.Quantity, cart.CreatedAt, cart.UpdatedAt)
	// helper.PanicIfError(err)

	var cartSlice []entity.Cart
	for _, cartValue := range carts {
		var id int
		err := statement.QueryRow(cartValue.UserId, cartValue.GadgetId, cartValue.Quantity, cartValue.CreatedAt, cartValue.UpdatedAt).Scan(&id)
		if err != nil {
			return cartSlice, err
		}

		cart := entity.Cart{
			Id:        id,
			UserId:    cartValue.UserId,
			GadgetId:  cartValue.GadgetId,
			Quantity:  cartValue.Quantity,
			CreatedAt: cartValue.CreatedAt,
			UpdatedAt: cartValue.UpdatedAt,
		}

		cartSlice = append(cartSlice, cart)
	}

	return cartSlice, nil
}

func (repository *cartRepository) Delete(ctx context.Context, cart entity.Cart, tx *sql.Tx) {
	query := "DELETE FROM carts WHERE id = $1"
	_, err := tx.ExecContext(ctx, query, cart.Id)
	helper.PanicIfError(err)
}

func (repository *cartRepository) DeleteByUserId(ctx context.Context, userId int, tx *sql.Tx) {
	query := "DELETE FROM carts WHERE user_id = $1"
	_, err := tx.ExecContext(ctx, query, userId)
	helper.PanicIfError(err)
}
