package helper

import "iceller-store/entity"

func GadgetResponse(gadget entity.Gadget) entity.Gadget {
	return entity.Gadget{
		Name:        gadget.Name,
		Price:       gadget.Price,
		Stock:       gadget.Stock,
		Description: gadget.Description,
		Storage:     gadget.Storage,
		RAM:         gadget.RAM,
		Color:       gadget.Color,
		CreatedAt:   gadget.CreatedAt,
		UpdatedAt:   gadget.UpdatedAt,
	}
}

func GadgetResponses(gadgets []entity.Gadget) []entity.Gadget {
	var gadgetsResponses []entity.Gadget
	for _, gadget := range gadgets {
		gadgetsResponses = append(gadgetsResponses, GadgetResponse(gadget))
	}

	return gadgetsResponses
}
