package helper

import (
	"encoding/json"
	"net/http"
)

func ErrorResponseHandler(writer http.ResponseWriter, code int, err error) {
	writer.Header().Add("Content-type", "application/json")
	writer.WriteHeader(code)
	webResponse := WebResponseTemplate("fail", code, err.Error(), nil)
	encoder := json.NewEncoder(writer)
	err = encoder.Encode(webResponse)
	PanicIfError(err)
}
