package helper

type WebResponse struct {
	Status  string      `json:"status"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func WebResponseTemplate(status string, code int, message string, data interface{}) WebResponse {
	return WebResponse{
		Status:  status,
		Code:    code,
		Message: message,
		Data:    data,
	}
}
