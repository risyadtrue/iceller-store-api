package entity

import "time"

type Cart struct {
	Id        int
	UserId    int
	GadgetId  int
	Quantity  int
	Total     int
	CreatedAt time.Time
	UpdatedAt time.Time
	User      User
	Gadget    Gadget
}
