package entity

import "time"

type Gadget struct {
	Id          int
	Name        string
	Price       int
	Stock       int
	Description string
	Storage     string
	RAM         string
	Color       string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
