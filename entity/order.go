package entity

import "time"

type Order struct {
	Id        int
	UserId    int
	GadgetId  int
	Total     int
	Quantity  int
	Status    string
	CreatedAt time.Time
	UpdatedAt time.Time
	Gadget    Gadget
}
