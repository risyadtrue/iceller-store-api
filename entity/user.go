package entity

import "time"

type User struct {
	Id        int
	Email     string
	Password  string
	FullName  string
	Role      string
	Status    bool
	CreatedAt time.Time
	UpdatedAt time.Time
}
