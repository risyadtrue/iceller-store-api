package web

type CartRequest struct {
	UserId   int `json:"user_id"`
	GadgetId int `json:"gadget_id"`
	Quantity int `json:"quantity"`
}

type CartRequestBulk struct {
	UserId               int
	BulkCartRequestValue []BulkCartRequestValue
}

type BulkCartRequestValue struct {
	GadgetId int `json:"gadget_id"`
	Quantity int `json:"quantity"`
}

//type BulkCartRequest struct {
//	GadgetId int
//	BulkCartRequestValue
//}
//
