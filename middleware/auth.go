package middleware

import (
	"context"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"iceller-store/helper"
	"iceller-store/service"
	"net/http"
	"strings"
)

type authMiddleware struct {
	authService service.AuthService
	userService service.UserService
}

func NewAuthMiddleware(authService service.AuthService, userService service.UserService) *authMiddleware {
	return &authMiddleware{authService: authService, userService: userService}
}

func (middleware *authMiddleware) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")
		ctx := context.Background()

		if !strings.Contains(authHeader, "Bearer") {
			webResponse := helper.WebResponseTemplate("failed", 401, "Unauthorized", nil)

			writer.Header().Add("Content-type", "application/json")
			encoder := json.NewEncoder(writer)
			err := encoder.Encode(webResponse)
			helper.PanicIfError(err)
			return
		}

		tokenSplit := strings.Split(authHeader, " ")
		tokenString := tokenSplit[1]

		token, err := middleware.authService.ValidateToken(tokenString)
		if err != nil {
			webResponse := helper.WebResponseTemplate("failed", 401, "Unauthorized", nil)

			writer.Header().Add("Content-type", "application/json")
			encoder := json.NewEncoder(writer)
			err := encoder.Encode(webResponse)
			helper.PanicIfError(err)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			webResponse := helper.WebResponseTemplate("failed", 401, "Unauthorized", nil)

			writer.Header().Add("Content-type", "application/json")
			encoder := json.NewEncoder(writer)
			err := encoder.Encode(webResponse)
			helper.PanicIfError(err)
			return
		}

		userId := int(claim["userId"].(float64))

		user, err := middleware.userService.GetById(ctx, userId)
		helper.PanicIfError(err)

		ctxValue := context.WithValue(request.Context(), "user", user)
		next.ServeHTTP(writer, request.WithContext(ctxValue))
	})
}
