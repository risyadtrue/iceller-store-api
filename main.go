package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	_ "github.com/lib/pq"
	"iceller-store/database"
	"iceller-store/handler"
	"iceller-store/helper"
	"iceller-store/middleware"
	"iceller-store/repository"
	"iceller-store/service"
	"net/http"
)

func main() {
	db := database.GetConnection()
	defer db.Close()

	validate := validator.New()
	gadgetRepository := repository.NewGadgetRepository(db)
	userRepository := repository.NewUserRepository(db)
	cartRepository := repository.NewCartRepository(db)
	orderRepository := repository.NewOrderRepository(db)

	gadgetService := service.NewGadgetService(gadgetRepository, db)
	userService := service.NewUserService(userRepository, validate)
	authService := service.NewAuthService()
	cartService := service.NewCartService(cartRepository, db, gadgetRepository)
	orderService := service.NewOrderService(orderRepository, cartRepository, gadgetRepository, db)

	gadgetHandler := handler.NewGadgetController(gadgetService)
	userHandler := handler.NewUserHandler(userService, authService)
	cartHandler := handler.NewCartHandler(cartService)
	orderHandler := handler.NewOrderHandler(orderService)

	authMiddleware := middleware.NewAuthMiddleware(authService, userService)

	r := chi.NewRouter()

	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("Welcome to iCeller-Store!!"))
	})

	r.Route("/api/v1", func(r chi.Router) {
		//r.Use(render.SetContentType(render.ContentTypeJSON))
		r.Get("/gadget", gadgetHandler.FindAll)
		r.Post("/gadget", gadgetHandler.FindByName)
		r.Post("/user/register", userHandler.Create)
		r.Post("/user/login", userHandler.LoginUser)
		r.With(authMiddleware.AuthMiddleware).Get("/user/me", userHandler.GetLoggedInUser)
		r.With(authMiddleware.AuthMiddleware).Post("/cart/{gadgetId}", cartHandler.AddToCart)
		r.With(authMiddleware.AuthMiddleware).Post("/carts", cartHandler.BulkAddToCart)
		r.With(authMiddleware.AuthMiddleware).Delete("/cart/{cartId}", cartHandler.DeleteCart)
		r.With(authMiddleware.AuthMiddleware).Post("/cart/checkout", orderHandler.CheckOut)
		r.With(authMiddleware.AuthMiddleware).Get("/orders/me", orderHandler.GetOrderHistory)
	})

	fmt.Println("Serve on localhost:3000")
	err := http.ListenAndServe(":3000", r)
	helper.PanicIfError(err)

}
